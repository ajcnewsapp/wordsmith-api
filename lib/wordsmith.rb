require 'wordsmith-ruby-sdk'
require 'builder'
require 'optparse'
require 'yaml'
require 'logger'
require 'time'
require 'securerandom'

loglevels = {
  info: Logger::INFO,
  debug: Logger::DEBUG
}

options = {}

options_parser = OptionParser.new do |opts|
  executable_name = File.basename($PROGRAM_NAME)
  opts.banner = "Retunrs Wordsmith API response and converts it to RSS

  Usage: #{executable_name} [options]"
  opts.on('-k KEY','--key KEY', "Wordsmiot API key") do |key|
    options[:key] = key
  end
  opts.on('-d DATAPATH','--data-path DATAPATH',"Path to JSON data file") do |data_path|
    options[:data_path] = data_path
  end
  opts.on('-p PROJECT','--project PROJECT',"Wordsmith project slug") do |project|
    options[:project] = project
  end
  opts.on('-c CONTENTTMPL','--content-tmpl CONTENTTMPL',"Wordsmith story content tempalte slug") do |content_tmpl|
    options[:content_tmpl] = content_tmpl
  end
  opts.on('-h HEADLINETMPL','--headline-tmpl HEADLINETMPL',"Wordsmith headline template slug") do |headline_tmpl|
    options[:headline_tmpl] = headline_tmpl
  end
  opts.on('-g LOGPATH','--log-path LOGPATH',"Path to log file") do |log_path|
    options[:log_path] = log_path
  end
  opts.on('-f CONFIGPATH','--config-path CONFIGPATH', "Config file. This will overwrite options set on command line.") do |config_file|
    options[:config_file] = config_file
  end
  opts.on('-o OUTFILE', "Output file") do |out_file|
    options[:out_file] = out_file
  end
end.parse!

CONFIG_FILE = options[:config_file] || File.join(ENV['HOME'], '.wordsmith_config.yaml')
if File.exists? CONFIG_FILE
  config_options = YAML.load_file(CONFIG_FILE)
  options.merge!(config_options)
end

path = options[:log_path] || File.expand_path(File.dirname(File.dirname(__FILE__)))
LOG = Logger.new(path + '/worsmith_api.log', 'monthly')
LOG.level = options[:log_level] || Logger::INFO

LOG.info("*** START ***")
LOG.debug('Using config file settings') if File.exists? CONFIG_FILE

Wordsmith.configure do |config|
  config.token = options[:key]
  config.url = 'https://api.automatedinsights.com/v1' #optional, this is the default value
end

project = Wordsmith::Project.find(options[:project])
template = project.templates.find(options[:content_tmpl])

f = File.read(options[:data_path])
data = JSON.parse(f)

# resp = template.generate(data[1])

rss = Builder::XmlMarkup.new
rss.instruct!
rss.rss :version => '2.0' do
  pubdate = Time.gm(*Time.now.to_a)
  rss.channel do
    rss.link 'http://ajc-wsfeed.s3-website-us-east-1.amazonaws.com/wsfeed.rss'
    rss.description 'School SAT results'
    rss.title 'SAT'
    rss.lastBuildDate pubdate
    rss.pubDate pubdate
  end

  data.each do |school|
    resp = template.generate(school)
    puts school['school']
    rss.item do
      rss.guid "#{SecureRandom.uuid}-#{school['id']}"
      rss.title "2017 SAT scores announced for #{school['school']}"
      rss.description resp[:content]
      rss.pubDate Time.gm(*Time.now.to_a)
      rss.category "/News/Local Education"
      rss.topic "schools-news,wordsmith"
      rss.byline "Staff"
      rss.source "Atlanta Journal-Constitution"
      rss.seoKeyWords "#{school['district']},#{school['school']},test scores,SAT,"
      rss.seoSerpDescription "2017 SAT results for #{school['school']} in #{school['district']}"
      rss.media(
        :url => 'http://www.myajc.com/rf/image_medium/Pub/p8/MyAJC/Blog/Ajc_Get_Schooled_Blog/2017/03/05/Images/photos.medleyphoto.13421855.jpg'
      )
    end
  end
end

File.open(options[:out_file], 'w') do |f|
  f.puts rss.to_s
end
