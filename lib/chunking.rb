require 'active_record'
require 'mysql2'
require 'builder'
require 'wordsmith-ruby-sdk'
require 'yaml'
require 'time'
require 'securerandom'
require 'aws-sdk'
require 'logger'

LOG = Logger.new('log/wordsmith.log')
LOG.level = Logger::INFO
links = <<END
<ul>
  <li>Check out our Ultimate Atlanta Schools Guide at <a href="http://schools.myajc.com/" target="_blank">schools.myajc.com</a></li>
  <li>Read our in-depth coverage of local school issues at <a href="http://www.myajc.com/education/" target="_blank">http://www.myajc.com/education/</a></li>
  <li>Maureen Downey offers insight on education policies. Read her blog at  <a href="http://getschooled.blog.myajc.com/" target="_blank">http://getschooled.blog.myajc.com/</a></li>
  <li>Are you a fan of high school sports? You should check out <a href="http://www.ajc.com/sports/high-school/" target="_blank">http://www.ajc.com/sports/high-school/</a></li>
</ul>
END

options = YAML.load_file('config/wordsmith_config.yaml')
LOG.debug( options )

Wordsmith.configure do |config|
  config.token = options[:key]
  config.url = 'https://api.automatedinsights.com/v1' #optional, this is the default value
end
project = Wordsmith::Project.find(options[:project])
template = project.templates.find(options[:content_tmpl])

begin
  ActiveRecord::Base.establish_connection(
    adapter: "mysql2",
    host: options[:db_host],
    username: options[:db_username],
    password: options[:db_password],
    database: options[:db_database]
  )

  $table = options[:db_table] # Save table name to global so it's available in Class scope. There has to be a better way.
  class Dbase < ActiveRecord::Base
    self.table_name = $table
    self.primary_key = 'id'
  end
rescue => e
  LOG.errror(e.message)
  LOG.error(e.backtrace)
  raise
end


limit = options[:limit]
offset = nil
f = File.open('config/offset.cfg','r')
begin
  i = f.readline.to_i
rescue  # if file is blank
  i = 0
end
f.close

exit(true) if i == 999
LOG.info("offset: #{i}")

offset = (limit * i)
puts "limit: #{limit}, offset: #{offset}"
i+=1

f = File.open('config/offset.cfg','w')

data = Dbase.limit(limit).offset(offset)
LOG.info("#{data.length} records found")
i = 999 if data.length < limit

File.open('wsfeed.rss','w') do |out|
  rss = Builder::XmlMarkup.new
  rss.instruct!
  rss.rss :version => '2.0' do
    pubdate = Time.gm(*Time.now.to_a)
    rss.channel do
      rss.link 'http://ajc-wsfeed.s3-website-us-east-1.amazonaws.com/wsfeed.rss'
      rss.description 'School SAT results'
      rss.title 'SAT'
      rss.lastBuildDate pubdate
      rss.pubDate pubdate
    end

    data.each do |school|
      LOG.debug( school[:school] )
      resp = template.generate(school)
      content = resp[:content]
      content += links
      rss.item do
        rss.guid "#{school['id']}-SAT-2016"
        rss.title "Most recent SAT scores let #{school['school']} students know where they rank"
        rss.description do
          rss.cdata! content
        end
        rss.pubDate Time.gm(*Time.now.to_a)
        rss.category "/News/Local Education"
        rss.topic "schools-news,wordsmith"
        rss.byline "Staff"
        rss.source "Atlanta Journal-Constitution"
        rss.seoKeyWords "#{school['district']},#{school['school']},test scores,SAT,"
        rss.seoSerpDescription "2015-2016 SAT results for #{school['school']} in #{school['district']}"
        rss.seoTitle "#{school['school']} SAT scores"
        rss.media(
          :url => options[:image]
        )
      end
    end
  end
  rss_string = rss.to_s
  rss_string.gsub!(/\<to_s\/\>/, '')

  begin
    s3 = Aws::S3::Resource.new(region: 'us-east-1')
    obj = s3.bucket('ajc-wsfeed').object('test.rss')
    obj.put(body: rss_string, content_type: 'text/rss+xml')
    LOG.info("RSS uploaded to #{obj.key} in bucket #{obj.bucket.name}. Content length: #{obj.content_length}")
  rescue => e
    LOG.error(e.message)
    LOG.error(e.backtrace)
    raise e
  end
  out.puts rss_string
end

f.write i
f.close
